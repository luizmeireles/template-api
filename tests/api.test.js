// External libraries
const request = require("supertest");

// API module
const API = require("../src/api");

describe("The project", () => {
  let api, mockColl, mockDb, mongoClient, stanConn;
  const corsOptions = { origin: "*" };

  const secret = "SUPERSECRET";

  beforeEach(() => {
    mockColl = {
      insertOne: jest.fn(),
      findOne: jest.fn(),
      deleteOne: jest.fn(),
    };

    mockDb = { collection: () => mockColl };

    mongoClient = { db: () => mockDb };

    stanConn = { publish: jest.fn() };

    api = API(corsOptions, { mongoClient, stanConn, secret });
  });

  it("can use Jest", () => {
    expect(true).toBe(true);
  });

  it("can use Supertest", async () => {
    const response = await request(api).get("/");
    expect(response.status).toBe(200);
    expect(response.body).toBe("Hello, World!");
  });

  it("can use CORS", async () => {
    const response = await request(api).get("/");
    const cors_header = response.header["access-control-allow-origin"];
    expect(cors_header).toBe("*");
  });

  it("creates an user", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(201);
    expect(response.body.user).toBeDefined();
    expect(stanConn.publish).toHaveBeenCalled();
    expect(mockColl.findOne).toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("can not create user if name is empty", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
  });

  it("can not create user if email is malformed", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
  });

  it("can not create user if email is already taken", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    mockColl.findOne.mockResolvedValueOnce({
      name: "foo",
      email: "foo@example.com",
      password: "12356abcde",
      id: "fooba",
    });

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
  });

  it("can not create user if password length is less than 8 characteres", async () => {
    const pw = "123";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
  });

  it("can not create user if password length is greater than 32 characteres", async () => {
    const pw = "J9PrM2G9JnaU5KH7ESPngsUbudhnwBmWA";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
  });

  it("can not create user if password and passwordConfirmation are different", async () => {
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: "123456foo",
      passwordConfirmation: "123456baa",
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(422);
  });

  it("allows a registered user to delete its account", async () => {
    const uid = "608ef5cc069020a1d61d5380";
    const correctToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGVmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.DU55f1y8dGSJPWYXrHUUwU0zGc-N8FixQqontudI4RE";

    const response = await request(api)
      .delete(`/users/${uid}`)
      .set("Authorization", `Bearer ${correctToken}`);

    expect(response.status).toBe(200);
    expect(stanConn.publish).toHaveBeenCalled();
    expect(mockColl.deleteOne).not.toHaveBeenCalled();
  });

  it("blocks a registered user to delete anothers account", async () => {
    const uid = "608ef5cc069020a1d61d5380";
    const wrongToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGZmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.J7xckIJZlqkmZBomOG8CIBJPYYen7I8Mx3hUn1rVnWc";

    const response = await request(api)
      .delete(`/users/${uid}`)
      .set("Authorization", `Bearer ${wrongToken}`);

    expect(response.status).toBe(403);
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.deleteOne).not.toHaveBeenCalled();
  });
});
