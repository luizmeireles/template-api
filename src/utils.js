const TOPIC = "users";
const USER_CREATED = "UserCreated";
const USER_DELETED = "UserDeleted";
const userRespFields = ["id", "name", "email"];

const createEventBody = (eventType, entityId, entityAggregate) => {
  return {
    eventType,
    entityId,
    entityAggregate,
  };
};

const publishMessage = (context) => (req, resp, next) => {
  const event = resp.locals.event;
  const { stanConn } = context;

  stanConn.publish(TOPIC, JSON.stringify(event), (err, guid) => {
    if (err) {
      console.log("publish failed: " + err);
    } else {
      console.log("published message with guid: " + guid);
    }
  });

  next();
};

module.exports = {
  TOPIC,
  USER_CREATED,
  USER_DELETED,
  userRespFields,
  createEventBody,
  publishMessage,
};
