const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");
const { validateUserCreation, validateUserDeletion } = require("./validation");
const {
  createEventBody,
  publishMessage,
  USER_DELETED,
  USER_CREATED,
  userRespFields,
} = require("./utils");

module.exports = function (corsOptions, context) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", validateUserCreation(context), async (req, resp, next) => {
    const user = req.body;
    const userDoc = {
      ...user,
      id: uuid(),
    };

    const event = createEventBody(USER_CREATED, userDoc["id"], userDoc);

    const userResp = userRespFields
      .map((k) => ({ [k]: userDoc[k] }))
      .reduce((res, o) => Object.assign(res, o), {});

    resp.locals.event = event;
    resp.status(201).send({ user: userResp });

    next();
  });

  api.delete(
    "/users/:uuid",
    validateUserDeletion(context),
    async (req, resp, next) => {
      const userId = req.params["uuid"];
      const event = createEventBody(USER_DELETED, userId, {});

      resp.locals.event = event;
      resp.status(200).send({ id: userId });

      next();
    }
  );

  api.use(publishMessage(context));

  return api;
};
