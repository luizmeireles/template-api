const jwt = require("jsonwebtoken");

const validateUserName = (name) => name.length > 0;

const validateUserEmail = (email) => {
  if (email.length > 0) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  return false;
};

const validateUserPassword = (password) => {
  if (password.length >= 8 && password.length <= 32) {
    const re = /^[a-z0-9]+$/i;
    return re.test(password);
  }

  return false;
};

const userFieldsValidation = {
  name: validateUserName,
  email: validateUserEmail,
  password: validateUserPassword,
  passwordConfirmation: validateUserPassword,
};

const validateUserCreation = (context) => async (req, resp, next) => {
  const user = req.body;
  const { mongoClient } = context;

  // Check for missing field
  for (let field in userFieldsValidation) {
    if (!user[field]) {
      resp.status(400).send({
        error: `Request body had missing field ${field}`,
      });
      return;
    }
  }

  // Validate fields
  for (let field in userFieldsValidation) {
    if (!userFieldsValidation[field](user[field])) {
      resp.status(400).send({
        error: `Request body had malformed field ${field}`,
      });
      return;
    }
  }

  // Check if user email is arealdy in the database
  const usersCollection = mongoClient.db("pingr").collection("users");
  const userFound = await usersCollection.findOne({ email: user["email"] });

  if (userFound) {
    resp.status(400).send({
      error: "Request body had malformed field email",
    });
    return;
  }

  // Semantic validation for password and passwordConfirmation
  if (user["password"] !== user["passwordConfirmation"]) {
    resp.status(422).send({
      error: "Password confirmation did not match",
    });
    return;
  }

  next();
};

const validateUserDeletion = (context) => (req, resp, next) => {
  const { secret } = context;
  const bearerHeader = req.headers["authorization"];
  const userId = req.params["uuid"];

  if (!bearerHeader) {
    return resp.status(401).send({
      error: "Access Token not found",
    });
  }

  const bearer = bearerHeader.split(" ");
  const bearerToken = bearer[1];
  const token = jwt.verify(bearerToken, secret);

  if (token["id"] !== userId) {
    resp.status(403).send({
      error: "Access Token did not match User ID",
    });
    return;
  }

  next();
};

module.exports = { validateUserCreation, validateUserDeletion };
