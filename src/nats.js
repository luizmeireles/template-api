const stan = require("node-nats-streaming");

const USER_CREATED = "UserCreated";
const USER_DELETED = "UserDeleted";

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  const usersCollection = mongoClient.db("pingr").collection("users");

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe("users", opts);

    subscription.on("message", async (msg) => {
      const { eventType, entityId, entityAggregate } = JSON.parse(
        msg.getData()
      );
      switch (eventType) {
        case USER_CREATED:
          await usersCollection.insertOne(entityAggregate);
          break;
        case USER_DELETED:
          await usersCollection.deleteOne({ id: entityId });
          break;
        default:
          break;
      }
    });
  });

  return conn;
};
